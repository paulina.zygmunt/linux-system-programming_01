Zadanie:
1) W obrebie 1 programu stworzyc 2 procesy przy uzyciu fork.
Proces 1 (rodzic) wczytuje zmienna x (scanf).
2) Procesy komunikuja sie ze soba za pomoca potoku unnamed pipe: 
Proces 1 (rodzic) wysyla do procesu 2 (dziecka) zmienna 'x'.
Proces 2 mnozy *2 zmienna 'x'.
3) Proces 2 wysyla wynik zapisany pod zmienna 'y' do procesu 3 (osobny program) 
za pomoca potoku named pipe (mkfifo). 

Nalezy:
- skompilowac prog1.c i prog2.c wpisujac w linii polecen:
gcc -o prog1 prog1.c && gcc -o prog2 prog2.c
- nastepnie w jednym terminalu uruchomic prog1: 
./prog1
- postepowac zgodnie z wyswietlanymi poleceniami i informacjami
- w drugim terminalu uruchomic prog2:
./prog2