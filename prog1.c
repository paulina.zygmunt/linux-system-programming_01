#include <stdio.h> 
#include <stdlib.h> // EXIT_SUCCESS
#include <unistd.h> // pipe, read, write
#include <sys/types.h> // wait
#include <sys/wait.h> // wait
#include <sys/stat.h> // mkfifo
#include <fcntl.h> // mkfifo


int main(int argc, char* argv[]) 
{ 
	int x, y;
	int status;
	int pid;
	int pipes_id[2]; 
		//pipes_id[0] - read
		//pipes_id[1] - write

	if ( pipe(pipes_id) == 0 ) 
	{
		pid = fork();

		if(pid == -1) 
		{
			exit(EXIT_FAILURE);
		}

		else if (pid > 0) 
		{ //parent
			close(pipes_id[0]); // close read
			printf("--- Process No. 1 ---\n");
			printf("Enter an integer x ");
			printf("(range from -64 to 63): ");
			scanf("%d", &x);
			write(pipes_id[1], &x, sizeof(x) );
			close(pipes_id[1]);

			wait(NULL);
			
			exit(EXIT_SUCCESS);
		}


		else if (pid == 0) 
		{ //child
			close(pipes_id[1]); // close write
			read(pipes_id[0], &x, BUFSIZ);
			close(pipes_id[0]);
			printf("\n--- Process No. 2 ---\n");
			printf("You entered x = %d.\n", x);
			y = 2 * x;
			printf("Waiting for the second program.\n");
			//mkfifo
			int fd;
			char *myfifo = "/tmp/myfifo";

			mkfifo(myfifo, 0666);
			fd = open(myfifo, O_WRONLY);
			printf("Sent y = 2*x = %d.\n", y);
			write(fd, &y, sizeof(y) );
			close(fd);
			unlink(myfifo);
			//end mkfifo

			exit(EXIT_SUCCESS);
		}
	}
}
